PANDOC=../pandoc
LUA=${PANDOC}/lua
MD=vkr
TEMPLATE=print
pandoc ${MD}.md -o out/${MD}.docx -f markdown-markdown_in_html_blocks-blank_before_blockquote \
  -L ${LUA}/parse-html.lua -L ${LUA}/include-files.lua -L ${LUA}/pagebreak.lua \
  -L ${LUA}/docx-toc.lua -L ${LUA}/docx-newsection.lua \
  -F pandoc-crossref --reference-doc ${PANDOC}/${TEMPLATE}.docx

osascript -l JavaScript -e '
var Word = Application("Microsoft Word");
Word.includeStandardAdditions = true;
if (Word.documents.whose({name: "example.docx"}).size()) {
  var doc = Word.documents.byName("example.docx").close();
}
Word.open("/Users/stolbovd/IdeaProjects/inkontext-doc/other/out/vkr.docx");
Word.activate();'


