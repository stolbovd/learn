SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

DROP DATABASE IF EXISTS learn;
DROP ROLE IF EXISTS learn;

CREATE ROLE learn
	LOGIN PASSWORD 'learnpassword'
	CREATEDB CREATEROLE
	VALID UNTIL 'infinity';

GRANT learn TO postgres;

CREATE DATABASE learn
	WITH OWNER = learn
	ENCODING = 'UTF8'
	CONNECTION LIMIT = -1;

create schema learn;
alter schema learn owner to learn;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE EXTENSION IF NOT EXISTS postgres_fdw;
CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA learn;
CREATE EXTENSION IF NOT EXISTS rum;

GRANT pg_monitor TO learn;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO learn;
GRANT ALL PRIVILEGES ON SCHEMA public TO learn;


