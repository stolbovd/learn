# Обработка данных в PostgreSQL

## Модель предметной области
### Domain Driven Design

**Модель-предметной области** (domain model) - текстовое описание предметной области аннотированное пояснительными моделями и диаграммами классов
 
1. Сущность - наличие идентичности
2. Объект-значение - определяется своими атрибутами
3. Агрегат - композиция классов с корневой сущностью
4. Сервис - только поведение

### UML Отношения на примере дерева
- [ ] mermaid

![domain model Дерева.png](media/domain%20model%20Дерева.png)

1. Ассоциация (один к одному)
2. Композиция (один ко многим)
3. Агрегация (многие ко многим)
4. Наследование (extend обобщение)
5. Реализация интерфейса (прилагательное)
6. Зависимость
7. Простая ассоциация

### Наша модель
![Learn.svg](media/Learn.svg)

## Инструменты для обработки данных
1. RegExp для обработки текста
2. Excel для обработки таблиц
3. SQL для работы с БД
4. Markdown для документирования

## RegExp
[RegExp.pdf](media/RegExp.pdf)

## DB Objects
1. User (Role), Database, Schema
2. Table
   - primary key
   - foreign key
   - index
   - trigger
   - constrains
3. Sequence
4. View and Materialized view
5. Functions SQL и PlPgSql

## SQL PostgreSQL

### CRUD
#### DDL create scheme learn
```postgresql
drop schema if exists learn cascade;
create schema learn;
alter schema learn owner to learn;

create table learn.learn_locus
(
    llc_id      serial,
    llc_name    text,
    llc_locus   bigint constraint llc_locus_fk references learn.locus,
    llc_template_cnt integer,
    llc_created timestamp
);
alter table learn.learn_locus owner to learn;

select * from learn.source_template;

```

#### Основные CRUD-операции
```postgresql
-- insert values
insert into learn.learn_locus (llc_id, llc_name, llc_locus, llc_created) values 
    (nextval('learn.learn_locus_llc_id_seq'), 'ссылка на локус', 1, now());
-- insert select 
insert into learn.learn_locus (llc_name, llc_locus, llc_created) 
    select 'Ссылка на '||loc_name, loc_id, now()
        from learn.locus;

-- select
select *
    from learn.learn_locus
    order by llc_id;
--    where llc_id = 1;
select src_locus, count(*) cnt
    from locus.source_template
    group by src_locus;

-- update fields
update learn.learn_locus
    set llc_name = 'измененная ссылка на локус',
        llc_created = now()
    where llc_id = 1;
-- update by subquery
update learn.learn_locus
    set llc_template_cnt = template.cnt
    from (select src_locus, count(*) cnt
            from locus.source_template
            group by src_locus) as template
    where template.src_locus = llc_locus;

delete from learn.learn_locus
    where llc_template_cnt isnull;

truncate learn.learn_locus;
alter sequence learn.learn_locus_llc_id_seq restart;
```

#### Транзакции
```postgresql
begin transaction;

rollback;
commit;
```

### SQL select

#### join tables
1. join
2. left join
3. right join
4. full join
5. outer join

- [ ] Круги эйлера

#### = like и regexp
```postgresql
--like
select * from learn.learn_locus
where llc_name like '%_ценка%'
order by llc_id;

--regexp_replace
select name_gp, trim(regexp_replace(name_gp, '[\s\u00a0\u180e\u2007\u200b-\u200f\u202f\u2060\ufeff]+', ' ', 'g'))
    from gosprogrammi_mfso.fact_of_indicators
    where name_gp not like trim(regexp_replace(name_gp, '[\s\u00a0\u180e\u2007\u200b-\u200f\u202f\u2060\ufeff]+', ' ', 'g'));

--regexp_matches
select * from indicators.dimension_code;
select dmc_id, regexp_matches(dmc_name, 'Индекс (.*) цен (.*)', 'g') name
-- select dmc_id, unnest(regexp_matches(dmc_name, 'Индекс (.*) цен (.*)', 'g')) name
    from indicators.dimension_code
    where dmc_name like 'Индекс%цен%';
```

#### with
```postgresql
with filter as (select src_options->'filter' as filter
                from locus.source_template where src_id = 6),
     filter_keys as (select jsonb_object_keys(filter) as dim from filter),
     dim as (select dimension.* from filter_keys join indicators.dimension on dim_code = dim),
     codes as (select dim, jsonb_array_elements_text(indicators.jsonb_array(filter->dim)) code, dim_id
               from filter_keys, filter, dim
               where dim = dim.dim_code),
     dmc as (select codes.*, dmc_id from codes
         join indicators.dimension_code dmc on dmc_dimension = dim_id and dmc_code = code),
     dmcs as (select dim, array_agg(dmc_id) as dmc from dmc group by dim),
     cond as (select string_agg('''{' || array_to_string(dmc, ',') || '}'' && dmcs', ' and ') cond from dmcs)
select coalesce(cond, 'false') from cond;
```

#### arrays
```postgresql
set search_path to indicators;

--Таблица в массив
with dim_dmc as (select dim_code, array_agg(DISTINCT dmc_code) as dmc
    from indicators.dimension_code
        inner join indicators.dimension on dim_id = dmc_dimension
    group by dim_code)
-- select array_to_string(dmc, ' ')
select dim_code, unnest(dmc)
    from dim_dmc
    where dmc @> ARRAY['102','105'];
-- select string_agg('''{' || array_to_string(dmc, ',') || '}'' && dmcs', ' and ') cond
-- from dim_dmc;
--     where '01' = any(dmc);

select ARRAY[5,8,9] && ARRAY[1,7,4,5,6,7,9], ARRAY[5,7,9] && ARRAY [3,1,2,8];

select string_agg('''{' || array_to_string(dmc, ',') || '}'' && dmcs', ' and ') cond from dmcs;

select '{9507, 9508, 9509}'::int[] as dmcs;

-- Массив в таблицу
select unnest('{9507, 9508, 9509}'::int[]) as dmc_id;
```

#### jsonb
```postgresql
select *
from locus.source_template,
     jsonb_array_elements(src_options -> 'rows') rows
where rows -> 'dimCodes' ->> 's_indicator_group' = 'GR1';

with src as (select src_locus, count(*) cnt, array_agg(src_id) templates
             from locus.source_template
             group by src_locus)
-- select * from src;
select json_build_object(
         'locus', src_locus,
         'templates', array_to_json(templates)) as loc_options
    from src;

select *
from  jsonb_array_elements(src_options -> 'rows') rows,
      jsonb_array_elements(src_options -> 'cols') cols;

select *
from locus.source_template
where exists (
         select true
         from  jsonb_array_elements(src_options -> 'rows') rows,
               jsonb_array_elements(src_options -> 'cols') cols);
```

#### regexp и functions sql и pgsql
```postgresql
create or replace function learn.simplify_text(str text) returns text
    language sql
as
$$
select trim(regexp_replace(str, '[\s\u00a0\u180e\u2007\u200b-\u200f\u202f\u2060\ufeff]+', ' ', 'g'))
$$;
alter function learn.simplify_text(text) owner to medural;

select name_gp, learn.simplify_text(name_gp)
    from gosprogrammi_mfso.fact_of_indicators
    where name_gp not like learn.simplify_text(name_gp);
```

#### recursive
```postgresql
with recursive search_graph(dmr_from, dmr_to, depth, path) AS (
    select g.dmr_from, g.dmr_to, 1 as depth, ARRAY [g.dmr_from] as path
        from indicators.dimension_relation as g
--   WHERE dmr_from = 8891 -- старт
    union all 
    select g.dmr_from, g.dmr_to, sg.depth + 1 as depth, g.dmr_from || path as path
        from indicators.dimension_relation as g, search_graph as sg
        where g.dmr_from = sg.dmr_to and (g.dmr_from <> all(sg.path))),
dmc as (select dmc_id from indicators.dimension_code where dmc_code = 'UO')
-- select * from search_graph;
select *, path[depth] as dmc, dimension_code.*
    from search_graph join indicators.dimension_code on dmc_id = path[depth]
where dmr_from = (select dmc_id from dmc)
order by path;
```

#### Explain plain

### Import, Export

## Используемые в demo языки
1. Markdown
2. UML
3. DDL
4. SQL
5. PLPgSQL

## InfoQ Software Architecture and Design Trends Report - April 2023
![InfoQ Software Architecture and Design Trends Report - April 2023](https://imgopt.infoq.com/fit-in/1200x2400/filters:quality(80)/filters:no_upscale()/articles/architecture-trends-2023/en/resources/1Architecture-2023-1680611288788.jpg)

## Эксперименты
```postgresql
alter table learn.learn_locus
    add unique (llc_array);

```